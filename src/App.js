import './App.css';
import 'bulma/css/bulma.min.css';
import classNames from 'classnames';
import dayjs from 'dayjs';
import 'dayjs/plugin/relativeTime';
import 'dayjs/plugin/duration';
import 'dayjs/locale/es';

function App() {

    dayjs.locale('es');

    var historyWeight = [
        { weight: 109.9, date: new Date('2022-08-20') },
        { weight: 105.3, date: new Date('2022-09-20') },
        { weight: 104.1, date: new Date('2022-10-09') },
        { weight: 103, date: new Date('2022-10-16') },
        { weight: 101.7, date: new Date('2022-11-06') },
        { weight: 100.7, date: new Date('2022-11-13') },
        { weight: 99.85, date: new Date('2022-11-26') },
        { weight: 98.65, date: new Date('2022-12-10') },
        { weight: 98.6, date: new Date('2022-12-17') },
        { weight: 99.55, date: new Date('2022-12-25') },
        { weight: 98.45, date: new Date('2022-12-28') },
        { weight: 98.7, date: new Date('2022-12-31') },
        { weight: 98.55, date: new Date('2023-01-07') },
        { weight: 97.8, date: new Date('2023-01-14') },
        { weight: 96.75, date: new Date('2023-01-21') },
        { weight: 96.75, date: new Date('2023-01-28') },
        { weight: 96.95, date: new Date('2023-02-04') },
        { weight: 95.6, date: new Date('2023-02-11') },
        { weight: 95.7, date: new Date('2023-02-18') },
        { weight: 95, date: new Date('2023-02-25') },
        { weight: 95.5, date: new Date('2023-05-06') },
        { weight: 94.6, date: new Date('2023-05-13') },
        { weight: 93.7, date: new Date('2023-05-22') },
        { weight: 93.8, date: new Date('2023-05-27') },
        { weight: 94.1, date: new Date('2023-06-03') },
        { weight: 94.3, date: new Date('2023-06-10') },
        { weight: 93.90, date: new Date('2023-06-17') },
        { weight: 93.3, date: new Date('2023-06-25') },
        { weight: 92.95, date: new Date('2023-07-08') },
        { weight: 92.1, date: new Date('2023-07-15') },
        { weight: 91.8, date: new Date('2023-07-22') },
        { weight: 91.2, date: new Date('2023-07-29') },
        { weight: 91.7, date: new Date('2023-08-05') },
        { weight: 91.4, date: new Date('2023-08-12') },
        { weight: 91.5, date: new Date('2023-08-19') },
        { weight: 91.1, date: new Date('2023-08-26') },
        { weight: 90.9, date: new Date('2023-09-02') },
        { weight: 90.4, date: new Date('2023-09-09') },
        { weight: 90.1, date: new Date('2023-09-16') },
        { weight: 89.75, date: new Date('2023-09-23') },
        { weight: 88.95, date: new Date('2023-09-30') },
        { weight: 88.30, date: new Date('2023-10-08') },
        { weight: 88.00, date: new Date('2023-10-14') },
        { weight: 87.80, date: new Date('2023-10-21') },
        { weight: 87.55, date: new Date('2023-10-29') },
        { weight: 87.85, date: new Date('2023-11-04') },
        { weight: 87, date: new Date('2023-11-11') },
        { weight: 87.05, date: new Date('2023-11-18') },
        { weight: 86.60, date: new Date('2023-11-25') },
        { weight: 86.45, date: new Date('2023-12-02') },
        { weight: 86, date: new Date('2023-12-09') },
        { weight: 88.25, date: new Date('2024-01-13') },
        { weight: 87.75, date: new Date('2024-01-20') },
        { weight: 87.10, date: new Date('2024-01-27') },
        { weight: 86.50, date: new Date('2024-02-03') },
        { weight: 86.35, date: new Date('2024-02-10') },
        { weight: 86.05, date: new Date('2024-02-17') },
        { weight: 86.05, date: new Date('2024-02-24') },
        { weight: 85.70, date: new Date('2024-03-02') },
        { weight: 85.60, date: new Date('2024-03-09') },
        { weight: 85.65, date: new Date('2024-03-16') },
        { weight: 85.20, date: new Date('2024-03-23') },
        { weight: 85,    date: new Date('2024-03-30') },
        { weight: 85,    date: new Date('2024-04-06') },
        { weight: 85,    date: new Date('2024-04-13') },
        { weight: 84.50, date: new Date('2024-04-20') },
        { weight: 84.50, date: new Date('2024-04-27') },
        { weight: 84.30, date: new Date('2024-05-04') },
        { weight: 84.30, date: new Date('2024-05-11') },
        { weight: 84, date: new Date('2024-05-18') },
        { weight: 84.20, date: new Date('2024-05-25') },
        { weight: 84.55, date: new Date('2024-06-01') },
        { weight: 84, date: new Date('2024-06-08') },
        { weight: 84, date: new Date('2024-06-15') },
        { weight: 84.40, date: new Date('2024-06-29') },
        { weight: 83.90, date: new Date('2024-07-06') },
        { weight: 84, date: new Date('2024-07-13') },
        { weight: 83.85, date: new Date('2024-07-20') },
        { weight: 83.70, date: new Date('2024-07-27') },
        { weight: 83.50, date: new Date('2024-08-03') },
        { weight: 83.45, date: new Date('2024-08-10') },
        { weight: 83.50, date: new Date('2024-08-17') },
        { weight: 83.30, date: new Date('2024-08-24') },
        { weight: 83.50, date: new Date('2024-08-31') },
        { weight: 83.25, date: new Date('2024-09-07') },
        { weight: 83.10, date: new Date('2024-09-14') },
        { weight: 83.10, date: new Date('2024-09-21') },
        { weight: 83.15, date: new Date('2024-09-28') },
        { weight: 82.95, date: new Date('2024-10-05') },
        { weight: 83.15, date: new Date('2024-10-13') },
        { weight: 82.95, date: new Date('2024-10-19') },
        { weight: 82.95, date: new Date('2024-10-26') },
        { weight: 82.95, date: new Date('2024-11-03') },
        { weight: 83.20, date: new Date('2024-11-09') },
        { weight: 83.10, date: new Date('2024-11-16') },
        { weight: 82.90, date: new Date('2024-11-23') },
        { weight: 82.85, date: new Date('2024-11-30') },
        { weight: 82.95, date: new Date('2024-12-07') },
        { weight: 82.95, date: new Date('2024-12-14') },
        { weight: 82.95, date: new Date('2024-12-21') },
        { weight: 83.25, date: new Date('2024-12-28') },
        { weight: 83.25, date: new Date('2025-01-04') },
        { weight: 83.10, date: new Date('2025-01-11') },
        { weight: 83, date: new Date('2025-01-18') },
        { weight: 82.9, date: new Date('2025-01-25') },
        { weight: 82.95, date: new Date('2025-02-01') },
        { weight: 82.85, date: new Date('2025-02-08') },
    ];

    function calculateFirstWeightRecordDate() {
        let minValue = historyWeight[0].weight;
        let dateOfRecord = historyWeight[0].date;

        for (let i = 1; i < historyWeight.length; i++) {
            if (historyWeight[i].weight < minValue) {
                minValue = historyWeight[i].weight;
                dateOfRecord = historyWeight[i].date;
            }
        }

        return dateOfRecord;
    }

    function calculateCurrentRate() {
        let lastWeight = historyWeight[historyWeight.length - 1].weight;
        let firstWeight = historyWeight[historyWeight.length - 5].weight;
        let totalWeightLoss = firstWeight - lastWeight;
        let weightLoseRate = totalWeightLoss / 4;

        return roundNumber(weightLoseRate);
    }

    function calculateLastMonthWeightLost() {
        let lastWeight = historyWeight[historyWeight.length - 1].weight;
        let firstWeight = historyWeight[historyWeight.length - 5].weight;
        let totalWeightLost = firstWeight - lastWeight;

        return roundNumber(totalWeightLost);
    }

    function calculateTimeSinceStart() {
        let today = dayjs();
        let startDate = historyWeight[0].date;
        let durationInDays = today.diff(startDate, 'days');

        return durationInDays;
    }

    function calculateWeightLostSinceStart() {
        let lastWeight = historyWeight[historyWeight.length - 1].weight;
        let firstWeight = historyWeight[0].weight;
        let differenceWeight = firstWeight - lastWeight;

        return roundNumber(differenceWeight);
    }

    function calculateTimeToGoal(withGoalRate = false) {
        dayjs.extend(require('dayjs/plugin/relativeTime'));

        let lastWeight = historyWeight[historyWeight.length - 1].weight;
        let weightUntilGoal = lastWeight - goalWeight;
        let weeksUntilGoalAtThisRate = weightUntilGoal / currentRate;

        if (withGoalRate === true) {
            weeksUntilGoalAtThisRate = weightUntilGoal / goalRate;
        }

        let dateUntil = dayjs().add(weeksUntilGoalAtThisRate, 'week');
        let goalCompletionDate = dateUntil.fromNow(true);

        return goalCompletionDate;
    }

    function calculateTimeToWeight(weight) {
        dayjs.extend(require('dayjs/plugin/relativeTime'));

        let howMuchWeightIsNeeded = currentWeight - weight;
        let weeksUntilGoal = howMuchWeightIsNeeded / currentRate;

        let dateUntil = dayjs().add(weeksUntilGoal, 'week');
        let goalCompletionDate = dateUntil.fromNow(true);

        return goalCompletionDate;
    }

    function calculateWeightBetweenMilestones() {
        let initialWeight = historyWeight[0].weight;
        let weightUntilGoal = initialWeight - goalWeight;

        console.log(roundNumber(weightUntilGoal / 10));

        return roundNumber(weightUntilGoal / 10);
    }

    function calculateMilestones() {
        let initialWeight = historyWeight[0].weight;
        let milestonesSteps = calculateWeightBetweenMilestones();
        let initialMilestoneWeight = roundNumber(initialWeight - milestonesSteps, false, true);
        let isInitialMilestoneAchieved = currentWeight < initialMilestoneWeight;

        let initialMilestone = {
            weight: initialMilestoneWeight,
            isAchieved: isInitialMilestoneAchieved,
            isCurrent: false,
        };

        let milestones = [initialMilestone];
        let wasPreviouslyAchieved = false;

        for (let i = 0; i < 9; i++) {
            let lastMilestone = milestones[milestones.length - 1].weight;
            let newMilestone = roundNumber(lastMilestone - milestonesSteps, false, true);
            let isAchieved = currentWeight <= newMilestone;
            let isCurrentMilestone = wasPreviouslyAchieved === true && isAchieved === false;
            let milestone = { weight: newMilestone, isAchieved: isAchieved, isCurrent: isCurrentMilestone };

            wasPreviouslyAchieved = isAchieved;
            milestones.push(milestone);
        }

        return milestones;
    }

    function calculateNextMilestone() {
        let unfinishedMilestones = milestones.filter(function (milestone) {
            return milestone.isAchieved !== true
        });

        return unfinishedMilestones[0];
    }

    function calculatePercentageToWeight(weight) {
        let weightBetweenMilestones = calculateWeightBetweenMilestones();
        let initialWeightSinceCurrentMilestone = roundNumber(nextMilestone.weight + weightBetweenMilestones);
        let weightUntilRequiredWeight = roundNumber(initialWeightSinceCurrentMilestone - weight);
        let lostWeightSinceThisMilestone = roundNumber(initialWeightSinceCurrentMilestone - currentWeight);
        let percentageUntilNextMilestone = lostWeightSinceThisMilestone / weightUntilRequiredWeight * 100;

        return roundNumber(percentageUntilNextMilestone, true);
    }

    function MilestoneProgressBar({ milestone }) {
        let percentageAchieved = milestone.isAchieved ? 100 : 0;

        if (milestone.isAchieved === false) {
            percentageAchieved = calculatePercentageToWeight(milestone.weight);

            if (milestone.isCurrent === false) {
                percentageAchieved = 0;
            }
        }

        return <progress className={'progress is-medium ' + (milestone.isAchieved ? 'is-success' : 'is-danger')} value={percentageAchieved} max="100">{percentageAchieved}</progress>;
    }

    function roundNumber(number, truncateDecimals = false, roundToNearestInt = false) {
        if (roundToNearestInt) {
            return Math.round(number);
        }
        
        let roundedNumber = Math.round(number * 100) / 100;

        if (truncateDecimals) {
            roundedNumber = parseInt(roundedNumber);
        }

        return roundedNumber;
    }

    function calculateBMI(weight) {
        return (weight / Math.pow( (height/100), 2 )).toFixed(1);
    }

    function convertWeightStringToGramsIfNeeded(weightInKg, useGramsAbbreviation, useAbsoluteValue=true) {
        let gramsWord = useGramsAbbreviation ? ' gr' : ' gramos';
        let weightToDisplay = useAbsoluteValue ? Math.abs(weightInKg) : weightInKg;

        if (Math.abs(weightInKg) < 1) {
            return (weightToDisplay * 1000) + gramsWord;
        }

        return weightToDisplay + ' kg';
    }

    function calculateLastMonthsLost(monthsQuantity) {
        const now = new Date();
        const targetDate = new Date(now.setMonth(now.getMonth() - monthsQuantity));
        const sortedHistory = historyWeight.sort((a, b) => a.date - b.date);
    
        let weightBeforeTargetDate = null;
        for (let i = sortedHistory.length - 1; i >= 0; i--) {
            if (sortedHistory[i].date <= targetDate) {
                weightBeforeTargetDate = sortedHistory[i].weight;
                break;
            }
        }
    
        const latestWeight = sortedHistory[sortedHistory.length - 1].weight;
        const weightLost = weightBeforeTargetDate - latestWeight;
    
        return weightLost.toFixed(2);
    }

    var height = 157;
    var goalWeight = 70;
    var goalRate = 0.5;

    var reversedHistoryWeight = [...historyWeight].reverse();

    var currentWeight = historyWeight[historyWeight.length - 1].weight;
    var weightLostSinceStart = calculateWeightLostSinceStart();

    var currentRate = calculateCurrentRate();

    var daysSinceStart = calculateTimeSinceStart();
    var timeToGoal = calculateTimeToGoal();
    var timeToGoalWithGoalRate = calculateTimeToGoal(true);

    var milestones = calculateMilestones();
    var nextMilestone = calculateNextMilestone();
    // var percentageToMilestone = calculatePercentageToWeight(calculateNextMilestone().weight);

    var timeUntilNextGoal = calculateTimeToWeight(nextMilestone.weight);

    var dateOfLatestRecord = calculateFirstWeightRecordDate();

    var lastMonthWeightLost = calculateLastMonthWeightLost();

    return (
        <div className="App">
            <section className="hero is-small is-info mb-5">
                <div className="hero-body">
                    <p className="title">
                        El peso de Mamá
                    </p>
                    <p className="subtitle">
                        {currentWeight} kg ({calculateBMI(currentWeight)})
                    </p>
                </div>
            </section>

            <div className="container">

                <nav className="level is-mobile">
                    <div className="level-item has-text-centered">
                        <div>
                            <p className="heading">Kg's perdidos</p>
                            <p className="title is-5">{weightLostSinceStart} kg</p>
                        </div>
                    </div>
                    <div className="level-item has-text-centered">
                        <div>
                            <p className="heading">En dieta desde hace</p>
                            <p className="title is-5">{daysSinceStart} días</p>
                        </div>
                    </div>
                </nav>

                <nav className="level is-mobile">
                    <div className="level-item has-text-centered">
                        <div>
                            <p className="heading">Promedio semanal</p>
                            <p className="title is-5">{convertWeightStringToGramsIfNeeded(currentRate)}</p>
                        </div>
                    </div>
                    <div className="level-item has-text-centered">
                        <div>
                            <p className="heading">Próximo objetivo</p>
                            <p className="title is-5">{timeUntilNextGoal}</p>
                        </div>
                    </div>
                </nav>

                <nav className="level is-mobile">
                    <div className="level-item has-text-centered">
                        <div>
                            <p className="heading">últimos 3 meses</p>
                            <p className="title is-5">{convertWeightStringToGramsIfNeeded(calculateLastMonthsLost(3), true, false)}</p>
                        </div>
                    </div>
                    <div className="level-item has-text-centered">
                        <div>
                            <p className="heading">últimos 6 meses</p>
                            <p className="title is-5">{convertWeightStringToGramsIfNeeded(calculateLastMonthsLost(6), true, false)}</p>
                        </div>
                    </div>
                </nav>

                <article className={'message is-' + (lastMonthWeightLost > 0 ? 'success' : 'warning')}>
                    <div className="message-header">

                        {lastMonthWeightLost > 0 && <span>¡Excelente trabajo!</span>}
                        {lastMonthWeightLost <= 0 && <span>¡Sigue adelante!</span>}
                        
                        <span className="icon">
                            <i className="fas fa-shield-heart"></i>
                        </span>

                    </div>
                    
                    {lastMonthWeightLost > 0 && <div className="message-body">
                        Has perdido <strong>{convertWeightStringToGramsIfNeeded(lastMonthWeightLost)}</strong> en las últimas <strong>4 semanas</strong>. <br /><br />
                        Este es un gran avance y demuestra tu esfuerzo y compromiso. <br />
                        ¡Cada logro te acerca a tu objetivo final! <br />
                        Mantén tu entusiasmo y continúa con esta excelente actitud. <br />
                        ¡Sigue adelante, lo estás haciendo genial! 🌟💪
                    </div>}

                    {lastMonthWeightLost === 0 && <div className="message-body">
                        Estás pesando lo mismo que hace 4 semanas 😟... <br /><br />
                        Esto es solo un pequeño obstáculo en tu viaje
                        ¡Recuerda que cada esfuerzo suma y cada día es una nueva oportunidad para avanzar! 
                        Mantén tu enfoque y continúa con tu dedicación. <br />
                        ¡Tú eres capaz de lograrlo! 🌟💪
                    </div>}

                    {lastMonthWeightLost < 0 && <div className="message-body">
                        En las últimas <strong>4 semanas</strong>, has visto un aumento de <strong>{convertWeightStringToGramsIfNeeded(Math.abs(lastMonthWeightLost))}</strong>. <br /><br />
                        Esto es solo un pequeño obstáculo en tu viaje
                        ¡Recuerda que cada esfuerzo suma y cada día es una nueva oportunidad para avanzar! 
                        Mantén tu enfoque y continúa con tu dedicación. <br />
                        ¡Tú eres capaz de lograrlo! 🌟💪
                    </div>}

                </article>

                {/* disabled by the moment */}
                {/* <article className="message is-info">
                    <div className="message-header">
                        Próximo objetivo:
                        <span className="icon">
                            <i className="fas fa-shield-heart"></i>
                        </span>

                    </div>
                    <div className="message-body">
                        Tu próximo objetivo es llegar a <strong>{nextMilestone.weight}kg</strong>. <br />
                        Actualmente estás al <strong>{percentageToMilestone}%</strong>.
                        A este ritmo tardarás <strong>{timeUntilNextGoal}</strong> en lograrlo, ¡ánimos!
                    </div>
                </article> */}

                <table className="table is-fullwidth">
                    <thead>
                        <tr>
                            <th className="has-text-right">#</th>
                            <th className="has-text-right">IMC</th>
                            <th className="has-text-right">Objetivo</th>
                            <th className="has-text-right">Estado</th>
                            <th className="has-text-right" width="65">%</th>
                        </tr>
                    </thead>
                    <tbody>
                        {milestones.map((row, i) => {
                            const rowClass = classNames({
                                'milestoneRowSuccess': row.isAchieved,
                                'current-row': row.isCurrent,
                            });

                            return (
                                <tr key={i} className={rowClass}>
                                    <td className="has-text-right gray-number">{i + 1}</td>
                                    <td className="has-text-right gray-number">{calculateBMI(row.weight)}</td>
                                    <td className="has-text-right">{row.weight} kg</td>
                                    <td className="has-text-right">
                                        <MilestoneProgressBar milestone={row} />
                                    </td>
                                    <td className="has-text-right">
                                        { row.isAchieved ? '100%' : 
                                            (row.isCurrent ? calculatePercentageToWeight(row.weight) + '%' : '-')
                                        }
                                    </td>
                                </tr>);
                        })}
                    </tbody>
                </table>

                <article className="message is-primary">
                    <div className="message-header">
                        <p>Sabias que...?</p>
                        <span className="icon">
                            <i className="fas fa-circle-question"></i>
                        </span>
                    </div>
                    <div className="message-body">
                        Estás bajando a <strong>{convertWeightStringToGramsIfNeeded(currentRate)}</strong> por semana. A este ritmo tardarás <strong>{timeToGoal}</strong> en llegar a los <strong>{goalWeight} kg</strong> que te propusiste.
                    </div>
                </article>

                <article className="message is-info">
                    <div className="message-header">
                        <p>Y si cambiara el ritmo...?</p>
                        <span className="icon">
                            <i className="fas fa-circle-question"></i>
                        </span>
                    </div>
                    <div className="message-body">
                        Si te mantuvieras a una media de <strong>{convertWeightStringToGramsIfNeeded(goalRate)}</strong> por semana, tardarías tan sólo <strong>{timeToGoalWithGoalRate}</strong> en lograrlo.
                    </div>
                </article>

                <table className="table is-striped is-fullwidth">
                    <thead>
                        <tr>
                            <th className="has-text-right">Fecha</th>
                            <th className="has-text-right">Peso</th>
                            <th className="has-text-right">

                                <span className="icon">
                                    <i className="fas fa-caret-down"></i>
                                </span>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {reversedHistoryWeight.map((row, i) => {
                            let previousWeight = i < reversedHistoryWeight.length - 1 ? reversedHistoryWeight[i + 1].weight : null;
                            let didLoseWeight = row.weight < previousWeight;
                            let weightDifference = previousWeight - row.weight;
                            let isWeightRecord = dateOfLatestRecord === row.date;

                            // first item shows a weight lost
                            if (previousWeight === null) {
                                didLoseWeight = true;
                            }

                            return (
                                <tr key={i} className={isWeightRecord ? 'is-selected' : ''}>
                                    <td className="has-text-right">{row.date.toLocaleDateString('es-es')}</td>
                                    <td className="has-text-right">
                                        {row.weight} kg
                                    </td>
                                    <td className="has-text-right">
                                        {previousWeight !== null && (
                                            <div className={didLoseWeight ? 'increaseWeight' : 'decreaseWeight'}>
                                                <span>
                                                    {convertWeightStringToGramsIfNeeded(roundNumber(weightDifference), true)}
                                                </span>
                                                <span className={'ml-1 icon has-text-' + (didLoseWeight ? 'success' : 'danger')}>
                                                    <i className={'fas fa-circle-' + (didLoseWeight ? 'down' : 'up')}></i>
                                                </span>
                                            </div>
                                        )}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>

                <footer className="footer mt-5">
                    <div className="content has-text-centered">
                        <p>
                            <strong>Mom's weight</strong> by Marco.
                        </p>
                    </div>
                </footer>
            </div>

        </div>
    );
}

export default App;
